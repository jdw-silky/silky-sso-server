package cn.test;

import java.util.concurrent.TimeUnit;

/**
 * @author ListJiang
 * class
 * description 工具类1
 * datetime 2021/6/30 13:14
 */
public class TimeUtils {
    /**
     * 休眠指定秒数
     *
     * @param seconds 秒
     */
    public static void sleep(int seconds) {
        try {
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
