-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: 192.168.137.139    Database: sso
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `authorities`
--

DROP TABLE IF EXISTS `authorities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authorities` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(20) DEFAULT NULL,
  `authority` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authorities`
--

LOCK TABLES `authorities` WRITE;
/*!40000 ALTER TABLE `authorities` DISABLE KEYS */;
INSERT INTO `authorities` VALUES (2,'user1','ROLE_USER');
/*!40000 ALTER TABLE `authorities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group_authorities`
--

DROP TABLE IF EXISTS `group_authorities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_authorities` (
  `group_Id` int NOT NULL AUTO_INCREMENT,
  `authority` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`group_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_authorities`
--

LOCK TABLES `group_authorities` WRITE;
/*!40000 ALTER TABLE `group_authorities` DISABLE KEYS */;
/*!40000 ALTER TABLE `group_authorities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group_members`
--

DROP TABLE IF EXISTS `group_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_members` (
  `id` int NOT NULL AUTO_INCREMENT,
  `userName` varchar(20) DEFAULT NULL,
  `group_Id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_members`
--

LOCK TABLES `group_members` WRITE;
/*!40000 ALTER TABLE `group_members` DISABLE KEYS */;
/*!40000 ALTER TABLE `group_members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id` int NOT NULL AUTO_INCREMENT,
  `groupName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth2_authorization`
--

DROP TABLE IF EXISTS `oauth2_authorization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth2_authorization` (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `registered_client_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `principal_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `authorization_grant_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `attributes` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `state` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `authorization_code_value` blob,
  `authorization_code_issued_at` timestamp NULL DEFAULT NULL,
  `authorization_code_expires_at` timestamp NULL DEFAULT NULL,
  `authorization_code_metadata` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `access_token_value` blob,
  `access_token_issued_at` timestamp NULL DEFAULT NULL,
  `access_token_expires_at` timestamp NULL DEFAULT NULL,
  `access_token_metadata` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `access_token_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `access_token_scopes` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `oidc_id_token_value` blob,
  `oidc_id_token_issued_at` timestamp NULL DEFAULT NULL,
  `oidc_id_token_expires_at` timestamp NULL DEFAULT NULL,
  `oidc_id_token_metadata` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `refresh_token_value` blob,
  `refresh_token_issued_at` timestamp NULL DEFAULT NULL,
  `refresh_token_expires_at` timestamp NULL DEFAULT NULL,
  `refresh_token_metadata` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth2_authorization`
--

LOCK TABLES `oauth2_authorization` WRITE;
/*!40000 ALTER TABLE `oauth2_authorization` DISABLE KEYS */;
INSERT INTO `oauth2_authorization` VALUES ('06ae07e4-d50e-4a3d-b453-4cd228cac2ad','33af3fac-75fb-451b-b602-5bf20228f0c4','user1','authorization_code','{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest\":{\"@class\":\"org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest\",\"authorizationUri\":\"http://192.168.137.1:9000/oauth2/authorize\",\"authorizationGrantType\":{\"value\":\"authorization_code\"},\"responseType\":{\"value\":\"code\"},\"clientId\":\"messaging-client\",\"redirectUri\":\"http://127.0.0.1:8080/login/oauth2/code/messaging-client-oidc\",\"scopes\":[\"java.util.Collections$UnmodifiableSet\",[\"openid\"]],\"state\":\"RSVV67LV0_8ylzVtHBpNWeXlJ0iUl2Aty66RJnhMxuA=\",\"additionalParameters\":{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"nonce\":\"UQJqn3iSJPr-bTt71kre_mgNxYq7oKHsh9JNsT2UWww\"},\"authorizationRequestUri\":\"http://192.168.137.1:9000/oauth2/authorize?response_type=code&client_id=messaging-client&scope=openid&state=RSVV67LV0_8ylzVtHBpNWeXlJ0iUl2Aty66RJnhMxuA%3D&redirect_uri=http://127.0.0.1:8080/login/oauth2/code/messaging-client-oidc&nonce=UQJqn3iSJPr-bTt71kre_mgNxYq7oKHsh9JNsT2UWww\",\"attributes\":{\"@class\":\"java.util.Collections$UnmodifiableMap\"}},\"java.security.Principal\":{\"@class\":\"org.springframework.security.authentication.UsernamePasswordAuthenticationToken\",\"authorities\":[\"java.util.Collections$UnmodifiableRandomAccessList\",[{\"@class\":\"org.springframework.security.core.authority.SimpleGrantedAuthority\",\"authority\":\"ROLE_USER\"}]],\"details\":{\"@class\":\"org.springframework.security.web.authentication.WebAuthenticationDetails\",\"remoteAddress\":\"192.168.137.1\",\"sessionId\":\"D2AC22F3215764F942F1DF9FED844759\"},\"authenticated\":true,\"principal\":{\"@class\":\"org.springframework.security.core.userdetails.User\",\"password\":null,\"username\":\"user1\",\"authorities\":[\"java.util.Collections$UnmodifiableSet\",[{\"@class\":\"org.springframework.security.core.authority.SimpleGrantedAuthority\",\"authority\":\"ROLE_USER\"}]],\"accountNonExpired\":true,\"accountNonLocked\":true,\"credentialsNonExpired\":true,\"enabled\":true},\"credentials\":null},\"org.springframework.security.oauth2.server.authorization.OAuth2Authorization.AUTHORIZED_SCOPE\":[\"java.util.Collections$UnmodifiableSet\",[\"openid\"]]}',NULL,'8H6bDfS0ZfrkofiHw1P1MmEhpTO1oZ7iI72ma60zDsmXWlhE848UiFcjz68bs-U5koli5x5_vhO_tKkpe1Qhzrenws-tMxEiqTVFVsqbMVeZn0A8Jh72frFTBIaESzKa','2021-10-16 16:15:19','2021-10-16 16:20:19','{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"metadata.token.invalidated\":true}','eyJraWQiOiI2N2RkZDgyOC0yMmM1LTQzMDEtOWVhMS1hODZiYjY2OTkwMjAiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJ1c2VyMSIsImF1ZCI6Im1lc3NhZ2luZy1jbGllbnQiLCJuYmYiOjE2MzQzNzIxMTksInNjb3BlIjpbIm9wZW5pZCJdLCJpc3MiOiJodHRwOlwvXC8xOTIuMTY4LjEzNy4xOjkwMDAiLCJleHAiOjE2MzQzNzI0MTksImlhdCI6MTYzNDM3MjExOX0.bhEYahnwmCtyYZY8Yil78Gr9IF6vTEnmf6hTAn_1Hxq1z349iGyZhDqOPgw_YbBovdxAA-QZVilOOgaslxMBNrYdb3EwZWC87pd_CCHHhK6a6cqlcjqQq3OKPuxP7duTUXMgcAloOaXonSJ7wfEesg6WGV9pZBKm3b1sRG3Xzn_32d8NkISaec64LQnH6NSsC3nLqTlt2WD-i6fG99DEv2Mru5IfvJoi92aL2YLgyKSzgZUWrqTXZhAUpF2ZXH3zGQuL4-7RmlVIgI8-yERHIfKsJJJmIAW3ptfz4wvHT574ZBI6MJwFNLnamUtmOIiyU-qyIRBOLqc2g7rM0dE2Lw','2021-10-16 16:15:19','2021-10-16 16:20:19','{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"metadata.token.claims\":{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"sub\":\"user1\",\"aud\":[\"java.util.Collections$SingletonList\",[\"messaging-client\"]],\"nbf\":[\"java.time.Instant\",1634372119.016027000],\"scope\":[\"java.util.Collections$UnmodifiableSet\",[\"openid\"]],\"iss\":[\"java.net.URL\",\"http://192.168.137.1:9000\"],\"exp\":[\"java.time.Instant\",1634372419.016027000],\"iat\":[\"java.time.Instant\",1634372119.016027000]},\"metadata.token.invalidated\":false}','Bearer','openid','eyJraWQiOiI2N2RkZDgyOC0yMmM1LTQzMDEtOWVhMS1hODZiYjY2OTkwMjAiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJ1c2VyMSIsImF1ZCI6Im1lc3NhZ2luZy1jbGllbnQiLCJhenAiOiJtZXNzYWdpbmctY2xpZW50IiwiaXNzIjoiaHR0cDpcL1wvMTkyLjE2OC4xMzcuMTo5MDAwIiwiZXhwIjoxNjM0MzczOTE5LCJpYXQiOjE2MzQzNzIxMTksIm5vbmNlIjoiVVFKcW4zaVNKUHItYlR0NzFrcmVfbWdOeFlxN29LSHNoOUpOc1QyVVd3dyJ9.CeClrCSyCeERqGH7ztwO1_cw4nH7wAIjlVIFT9akXmO2McAdLQKJ7mOrdbV53Dk7DyteT1M8cn2wq4LL3lKW_aDxG0pC_5gJX-HE6VhQwPZdKLFwGfTjDWihLvLJvsgbY1kclgb2GWip9Ax38K20AjjFsE1ODkKzD0R_MI_7J1OyBUHQccD7qsj2bslpcOlFGy01d45NbJ6Q_PzRCVOvHt6CUhdVBhAl1Ot-k_tDt472k_9ynhm8rnnu2ow9_zX0qup07jIXukXGtFBDmg1PlOvwjDkSvZ4x6M_e_npyNk7vVxgdVbByIE1YRYcLXuk_6TVwLeNl2-cACTB21fQ5Mg','2021-10-16 16:15:19','2021-10-16 16:45:19','{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"metadata.token.claims\":{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"sub\":\"user1\",\"aud\":[\"java.util.Collections$SingletonList\",[\"messaging-client\"]],\"azp\":\"messaging-client\",\"iss\":[\"java.net.URL\",\"http://192.168.137.1:9000\"],\"exp\":[\"java.time.Instant\",1634373919.030024000],\"iat\":[\"java.time.Instant\",1634372119.030024000],\"nonce\":\"UQJqn3iSJPr-bTt71kre_mgNxYq7oKHsh9JNsT2UWww\"},\"metadata.token.invalidated\":false}','ovlPuNKvhYDUY4swe1jTC_CahNy0Y8Varq4WUBe6ukRqHwle02d9fWRsuFEL8nCwx5e2i6UstLjfRSYSIiVQBNGBfc9BD2i_UWl-1yb7crmebwCNvJp2QcFXehEk1zb-','2021-10-16 16:15:19','2021-10-16 17:15:19','{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"metadata.token.invalidated\":false}'),('160cc92f-a594-4e10-9f9f-658c7d68cac3','33af3fac-75fb-451b-b602-5bf20228f0c4','user1','authorization_code','{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest\":{\"@class\":\"org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest\",\"authorizationUri\":\"http://192.168.137.1:9000/oauth2/authorize\",\"authorizationGrantType\":{\"value\":\"authorization_code\"},\"responseType\":{\"value\":\"code\"},\"clientId\":\"messaging-client\",\"redirectUri\":\"http://127.0.0.1:8080/login/oauth2/code/messaging-client-oidc\",\"scopes\":[\"java.util.Collections$UnmodifiableSet\",[\"openid\"]],\"state\":\"nQQdZCiekLGHrLnM6CF3KcewIhAMcviDmxI4gCwWbu4=\",\"additionalParameters\":{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"nonce\":\"l-zmOrRSDtqosDXuo-WVOSofUzl5ESYGLWiqf9Jj180\"},\"authorizationRequestUri\":\"http://192.168.137.1:9000/oauth2/authorize?response_type=code&client_id=messaging-client&scope=openid&state=nQQdZCiekLGHrLnM6CF3KcewIhAMcviDmxI4gCwWbu4%3D&redirect_uri=http://127.0.0.1:8080/login/oauth2/code/messaging-client-oidc&nonce=l-zmOrRSDtqosDXuo-WVOSofUzl5ESYGLWiqf9Jj180\",\"attributes\":{\"@class\":\"java.util.Collections$UnmodifiableMap\"}},\"java.security.Principal\":{\"@class\":\"org.springframework.security.authentication.UsernamePasswordAuthenticationToken\",\"authorities\":[\"java.util.Collections$UnmodifiableRandomAccessList\",[{\"@class\":\"org.springframework.security.core.authority.SimpleGrantedAuthority\",\"authority\":\"ROLE_USER\"}]],\"details\":{\"@class\":\"org.springframework.security.web.authentication.WebAuthenticationDetails\",\"remoteAddress\":\"192.168.137.1\",\"sessionId\":\"47DA0BAFB41B7A36FBF211D22F4C3DF5\"},\"authenticated\":true,\"principal\":{\"@class\":\"org.springframework.security.core.userdetails.User\",\"password\":null,\"username\":\"user1\",\"authorities\":[\"java.util.Collections$UnmodifiableSet\",[{\"@class\":\"org.springframework.security.core.authority.SimpleGrantedAuthority\",\"authority\":\"ROLE_USER\"}]],\"accountNonExpired\":true,\"accountNonLocked\":true,\"credentialsNonExpired\":true,\"enabled\":true},\"credentials\":null},\"org.springframework.security.oauth2.server.authorization.OAuth2Authorization.AUTHORIZED_SCOPE\":[\"java.util.Collections$UnmodifiableSet\",[\"openid\"]]}',NULL,'vTC4g_nk52oHiQxe07k_-xyZuZwseBB39grCyo_dRVim-vJhzl-9ilzmuet6ZS7l7L_Yba1oj6bk4if_TUZTL7N3IQXboqopSauh7dFKSSqOybJQtWiLVhJC08s0MCzj','2021-10-16 16:14:33','2021-10-16 16:19:33','{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"metadata.token.invalidated\":true}','eyJraWQiOiI2N2RkZDgyOC0yMmM1LTQzMDEtOWVhMS1hODZiYjY2OTkwMjAiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJ1c2VyMSIsImF1ZCI6Im1lc3NhZ2luZy1jbGllbnQiLCJuYmYiOjE2MzQzNzIwNzMsInNjb3BlIjpbIm9wZW5pZCJdLCJpc3MiOiJodHRwOlwvXC8xOTIuMTY4LjEzNy4xOjkwMDAiLCJleHAiOjE2MzQzNzIzNzMsImlhdCI6MTYzNDM3MjA3M30.SPCj9mFa-moqci3yHWq_7ua73K96N56-FrTZoL6czbPx-HbD_N5IRgJCZn6gIX-_DEI4QMPX8qHud68QCJOwdS9Nof8HAArT0pe3KX2r4xSuwlolGssyUvZLYz26ZelZcsgfKYJoAfH9WD3p0H5xeUJULBWLrm1MWLFFcgsf5qm-07lT0qpcxZPY0aZyU1Kx__dp9a8w_7gTv__kEhDoWse--f0CV8oxJ9ouHGr26R1le1b_0q10t5R96KgUwXnxMASe2F3oC97zCNy5l6xOWXGrq-_lBc1T0nmG8frpdghqtCMG5vfCedWPE1ovNiCtiQfgVcadq40baUIwOnEwag','2021-10-16 16:14:34','2021-10-16 16:19:34','{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"metadata.token.claims\":{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"sub\":\"user1\",\"aud\":[\"java.util.Collections$SingletonList\",[\"messaging-client\"]],\"nbf\":[\"java.time.Instant\",1634372073.762922400],\"scope\":[\"java.util.Collections$UnmodifiableSet\",[\"openid\"]],\"iss\":[\"java.net.URL\",\"http://192.168.137.1:9000\"],\"exp\":[\"java.time.Instant\",1634372373.762922400],\"iat\":[\"java.time.Instant\",1634372073.762922400]},\"metadata.token.invalidated\":false}','Bearer','openid','eyJraWQiOiI2N2RkZDgyOC0yMmM1LTQzMDEtOWVhMS1hODZiYjY2OTkwMjAiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJ1c2VyMSIsImF1ZCI6Im1lc3NhZ2luZy1jbGllbnQiLCJhenAiOiJtZXNzYWdpbmctY2xpZW50IiwiaXNzIjoiaHR0cDpcL1wvMTkyLjE2OC4xMzcuMTo5MDAwIiwiZXhwIjoxNjM0MzczODczLCJpYXQiOjE2MzQzNzIwNzMsIm5vbmNlIjoibC16bU9yUlNEdHFvc0RYdW8tV1ZPU29mVXpsNUVTWUdMV2lxZjlKajE4MCJ9.LFND4N29INZhlohIG_8rCMJq_iFJ9p_dcKb7UVqXvKQEfH5dmRADxWvaUTXPnjoNm4MjJ3zg9BWAUgKBeHobfTolo1i9dekxXz2h7MJep3xZO3H6bWhuNxPONeE7D99dz3EhWB1Ex3XoGNabL7kIEQlbV1z1eqtDi-QENs59u0sz9jcM9QXxpmxCu7WpMXqQVjNTh1AHs01B0Jbay62YN_715Fp8evZvaGRalM0JOq4K5QJGCMzGU-55TkuaW6RIqkku3ScbASTDyIyc1_DHkhmwBcoUjs521ZXVn_EqoOGSGgSq7n8GqN9zjP_sKE1_lk-vEDk3rBMmexBVkp9tBA','2021-10-16 16:14:34','2021-10-16 16:44:34','{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"metadata.token.claims\":{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"sub\":\"user1\",\"aud\":[\"java.util.Collections$SingletonList\",[\"messaging-client\"]],\"azp\":\"messaging-client\",\"iss\":[\"java.net.URL\",\"http://192.168.137.1:9000\"],\"exp\":[\"java.time.Instant\",1634373873.824440700],\"iat\":[\"java.time.Instant\",1634372073.824440700],\"nonce\":\"l-zmOrRSDtqosDXuo-WVOSofUzl5ESYGLWiqf9Jj180\"},\"metadata.token.invalidated\":false}','vEF6SvcvMT2m3lIGZzbKs3mkRv5-JOubmcfa7UprC--dT72xJVqV-MwtkxQCDtYNqFKG6LkONPzzVWeEnqpabAf-_g4R6xncAuFGtRL59WqNzk3TJ8_aFyt9i-4TT8Vo','2021-10-16 16:14:34','2021-10-16 17:14:34','{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"metadata.token.invalidated\":false}'),('47e6e29e-ab17-4082-a94d-4ef798de9930','33af3fac-75fb-451b-b602-5bf20228f0c4','user1','authorization_code','{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest\":{\"@class\":\"org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest\",\"authorizationUri\":\"http://192.168.137.1:9000/oauth2/authorize\",\"authorizationGrantType\":{\"value\":\"authorization_code\"},\"responseType\":{\"value\":\"code\"},\"clientId\":\"messaging-client\",\"redirectUri\":\"http://127.0.0.1:8080/authorized\",\"scopes\":[\"java.util.Collections$UnmodifiableSet\",[\"message.read\",\"message.write\"]],\"state\":\"-u0lCbW-LR3jSs6DS4HB6APXomKNIgm3QQbZKK7Iy9c=\",\"additionalParameters\":{\"@class\":\"java.util.Collections$UnmodifiableMap\"},\"authorizationRequestUri\":\"http://192.168.137.1:9000/oauth2/authorize?response_type=code&client_id=messaging-client&scope=message.read%20message.write&state=-u0lCbW-LR3jSs6DS4HB6APXomKNIgm3QQbZKK7Iy9c%3D&redirect_uri=http://127.0.0.1:8080/authorized\",\"attributes\":{\"@class\":\"java.util.Collections$UnmodifiableMap\"}},\"java.security.Principal\":{\"@class\":\"org.springframework.security.authentication.UsernamePasswordAuthenticationToken\",\"authorities\":[\"java.util.Collections$UnmodifiableRandomAccessList\",[{\"@class\":\"org.springframework.security.core.authority.SimpleGrantedAuthority\",\"authority\":\"ROLE_USER\"}]],\"details\":{\"@class\":\"org.springframework.security.web.authentication.WebAuthenticationDetails\",\"remoteAddress\":\"192.168.137.1\",\"sessionId\":\"D2AC22F3215764F942F1DF9FED844759\"},\"authenticated\":true,\"principal\":{\"@class\":\"org.springframework.security.core.userdetails.User\",\"password\":null,\"username\":\"user1\",\"authorities\":[\"java.util.Collections$UnmodifiableSet\",[{\"@class\":\"org.springframework.security.core.authority.SimpleGrantedAuthority\",\"authority\":\"ROLE_USER\"}]],\"accountNonExpired\":true,\"accountNonLocked\":true,\"credentialsNonExpired\":true,\"enabled\":true},\"credentials\":null},\"org.springframework.security.oauth2.server.authorization.OAuth2Authorization.AUTHORIZED_SCOPE\":[\"java.util.HashSet\",[\"message.read\",\"message.write\"]]}',NULL,'UDrupNB7-3647aEwcCSZenLtRlUpieA5x-7R4X95CgegAy_t2bJe6CQcc2U3ZwdGCDi9dSSR7rAu88cnsVZoVmSi6yKlxwOW6cmS5bRW8vVtOS3-PqfKoMIUbAaVcdfu','2021-10-16 16:16:55','2021-10-16 16:21:55','{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"metadata.token.invalidated\":true}','eyJraWQiOiI2N2RkZDgyOC0yMmM1LTQzMDEtOWVhMS1hODZiYjY2OTkwMjAiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJ1c2VyMSIsImF1ZCI6Im1lc3NhZ2luZy1jbGllbnQiLCJuYmYiOjE2MzQzNzIyMTUsInNjb3BlIjpbIm1lc3NhZ2UucmVhZCIsIm1lc3NhZ2Uud3JpdGUiXSwiaXNzIjoiaHR0cDpcL1wvMTkyLjE2OC4xMzcuMTo5MDAwIiwiZXhwIjoxNjM0MzcyNTE1LCJpYXQiOjE2MzQzNzIyMTV9.GZXoy0Tg9YPj1STw-Q7wsuEHOIW3C1rLI4hE5Yf7jYe9hUZp4XrgKZpYstPXatc3seLveU7ROoaO6O75BcobHIrwTAtzpzO9uTiE5AyRYtN7_Z40tMCXo1z0VmROxHoXI6l67K5Jg56h2ldagzw64gpHe6gk5QJA51DMzsISK-bo43uw6Q4dtMKIrxhNujGYxl1X_EQ85PXbKLK1-tSieXlQuFRxEUD64IkPb9w6COMcKhR8KAblJc9NErHQssXUUxZoVjDQgNXJy-bP3eHmM3YHQUOt2u4ZCxwp81AkROGjP4uEus8X--KAl_753iPq3yxQqsaW7ysiPErzj6-L2A','2021-10-16 16:16:55','2021-10-16 16:21:55','{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"metadata.token.claims\":{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"sub\":\"user1\",\"aud\":[\"java.util.Collections$SingletonList\",[\"messaging-client\"]],\"nbf\":[\"java.time.Instant\",1634372215.237248700],\"scope\":[\"java.util.HashSet\",[\"message.read\",\"message.write\"]],\"iss\":[\"java.net.URL\",\"http://192.168.137.1:9000\"],\"exp\":[\"java.time.Instant\",1634372515.237248700],\"iat\":[\"java.time.Instant\",1634372215.237248700]},\"metadata.token.invalidated\":false}','Bearer','message.read,message.write',NULL,NULL,NULL,NULL,'Fxpiag7-ZSx9VTNnJI1sBhxp8Xpd_okqlN7RRTPOIZ_y0vz8OgyGjQaqpKhHJDHYEN3rh7fcvT7IzaT14iK64Gh0M28wT2JlyWayEIX-OHcMr0rrdguXZUBCkoVtePe-','2021-10-16 16:16:55','2021-10-16 17:16:55','{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"metadata.token.invalidated\":false}'),('85f808ba-75b4-4ad2-a9bf-48caa90da468','33af3fac-75fb-451b-b602-5bf20228f0c4','user1','authorization_code','{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"java.security.Principal\":{\"@class\":\"org.springframework.security.authentication.UsernamePasswordAuthenticationToken\",\"authorities\":[\"java.util.Collections$UnmodifiableRandomAccessList\",[{\"@class\":\"org.springframework.security.core.authority.SimpleGrantedAuthority\",\"authority\":\"ROLE_USER\"}]],\"details\":{\"@class\":\"org.springframework.security.web.authentication.WebAuthenticationDetails\",\"remoteAddress\":\"192.168.137.1\",\"sessionId\":\"47DA0BAFB41B7A36FBF211D22F4C3DF5\"},\"authenticated\":true,\"principal\":{\"@class\":\"org.springframework.security.core.userdetails.User\",\"password\":null,\"username\":\"user1\",\"authorities\":[\"java.util.Collections$UnmodifiableSet\",[{\"@class\":\"org.springframework.security.core.authority.SimpleGrantedAuthority\",\"authority\":\"ROLE_USER\"}]],\"accountNonExpired\":true,\"accountNonLocked\":true,\"credentialsNonExpired\":true,\"enabled\":true},\"credentials\":null},\"org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest\":{\"@class\":\"org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest\",\"authorizationUri\":\"http://192.168.137.1:9000/oauth2/authorize\",\"authorizationGrantType\":{\"value\":\"authorization_code\"},\"responseType\":{\"value\":\"code\"},\"clientId\":\"messaging-client\",\"redirectUri\":\"http://127.0.0.1:8080/login/oauth2/code/messaging-client-oidc\",\"scopes\":[\"java.util.Collections$UnmodifiableSet\",[\"openid\"]],\"state\":\"4Yw6O16654mk6STDl5hmgy1R4NTnZqFXKpYKBP6gpNY=\",\"additionalParameters\":{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"nonce\":\"wsVm0L9_IXFY3Epv6FF0TTfimYhtYNkmQbPn6rlvz8A\"},\"authorizationRequestUri\":\"http://192.168.137.1:9000/oauth2/authorize?response_type=code&client_id=messaging-client&scope=openid&state=4Yw6O16654mk6STDl5hmgy1R4NTnZqFXKpYKBP6gpNY%3D&redirect_uri=http://127.0.0.1:8080/login/oauth2/code/messaging-client-oidc&nonce=wsVm0L9_IXFY3Epv6FF0TTfimYhtYNkmQbPn6rlvz8A\",\"attributes\":{\"@class\":\"java.util.Collections$UnmodifiableMap\"}},\"org.springframework.security.oauth2.server.authorization.OAuth2Authorization.AUTHORIZED_SCOPE\":[\"java.util.Collections$UnmodifiableSet\",[\"openid\"]]}',NULL,'7GUG5JPwoHKneEp5nwzpIzyKeBzSpHqjbrK2oGzD28uHHfUSj78SPPECMnFPJMyOUHo_9NVn3wPJGhUmt3M6pS4QSDEOOdVBm7EMbQ-z8g9kXlOfi6JfA45MevMc8FLw','2021-10-16 16:14:33','2021-10-16 16:19:33','{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"metadata.token.invalidated\":false}',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `oauth2_authorization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth2_authorization_consent`
--

DROP TABLE IF EXISTS `oauth2_authorization_consent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth2_authorization_consent` (
  `registered_client_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `principal_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `authorities` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`registered_client_id`,`principal_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth2_authorization_consent`
--

LOCK TABLES `oauth2_authorization_consent` WRITE;
/*!40000 ALTER TABLE `oauth2_authorization_consent` DISABLE KEYS */;
INSERT INTO `oauth2_authorization_consent` VALUES ('33af3fac-75fb-451b-b602-5bf20228f0c4','user1','SCOPE_message.read,SCOPE_message.write');
/*!40000 ALTER TABLE `oauth2_authorization_consent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth2_registered_client`
--

DROP TABLE IF EXISTS `oauth2_registered_client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth2_registered_client` (
  `id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `client_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `client_id_issued_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `client_secret` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `client_secret_expires_at` timestamp NULL DEFAULT NULL,
  `client_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `client_authentication_methods` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `authorization_grant_types` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `redirect_uris` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `scopes` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `client_settings` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `token_settings` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth2_registered_client`
--

LOCK TABLES `oauth2_registered_client` WRITE;
/*!40000 ALTER TABLE `oauth2_registered_client` DISABLE KEYS */;
INSERT INTO `oauth2_registered_client` VALUES ('33af3fac-75fb-451b-b602-5bf20228f0c4','messaging-client','2021-10-16 11:54:00','{bcrypt}$2a$10$Ux/9elzIx6jNOXHMTbgw/evziHgkpteHcwwsQOlQqP8OhtUxIm3x2',NULL,'33af3fac-75fb-451b-b602-5bf20228f0c4','client_secret_basic','refresh_token,client_credentials,authorization_code','http://127.0.0.1:8080/authorized,http://127.0.0.1:8080/login/oauth2/code/messaging-client-oidc','openid,message.read,message.write','{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"settings.client.require-proof-key\":false,\"settings.client.require-authorization-consent\":true}','{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"settings.token.reuse-refresh-tokens\":true,\"settings.token.id-token-signature-algorithm\":[\"org.springframework.security.oauth2.jose.jws.SignatureAlgorithm\",\"RS256\"],\"settings.token.access-token-time-to-live\":[\"java.time.Duration\",300.000000000],\"settings.token.refresh-token-time-to-live\":[\"java.time.Duration\",3600.000000000]}'),('4fbe1944-2ec8-4204-99d5-6620d16e9071','messaging-client','2021-10-16 12:38:33','{bcrypt}$2a$10$ntCwcElCici4VS9TH4SyP.EJCEwHImzqYcCZ2qdpgGZUMGucW8aWK',NULL,'4fbe1944-2ec8-4204-99d5-6620d16e9071','client_secret_basic','refresh_token,client_credentials,authorization_code','http://127.0.0.1:8080/authorized,http://127.0.0.1:8080/login/oauth2/code/messaging-client-oidc','openid,message.read,message.write','{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"settings.client.require-proof-key\":false,\"settings.client.require-authorization-consent\":true}','{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"settings.token.reuse-refresh-tokens\":true,\"settings.token.id-token-signature-algorithm\":[\"org.springframework.security.oauth2.jose.jws.SignatureAlgorithm\",\"RS256\"],\"settings.token.access-token-time-to-live\":[\"java.time.Duration\",300.000000000],\"settings.token.refresh-token-time-to-live\":[\"java.time.Duration\",3600.000000000]}'),('5de9ef2a-a284-46bf-8470-e5784a4e9bb1','messaging-client','2021-10-16 11:56:15','{bcrypt}$2a$10$u85B/pjeL3swb87J1Rb3EeJxlDRD1SCFH7cVXnObeUEelBhNFrVf6',NULL,'5de9ef2a-a284-46bf-8470-e5784a4e9bb1','client_secret_basic','refresh_token,client_credentials,authorization_code','http://127.0.0.1:8080/authorized,http://127.0.0.1:8080/login/oauth2/code/messaging-client-oidc','openid,message.read,message.write','{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"settings.client.require-proof-key\":false,\"settings.client.require-authorization-consent\":true}','{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"settings.token.reuse-refresh-tokens\":true,\"settings.token.id-token-signature-algorithm\":[\"org.springframework.security.oauth2.jose.jws.SignatureAlgorithm\",\"RS256\"],\"settings.token.access-token-time-to-live\":[\"java.time.Duration\",300.000000000],\"settings.token.refresh-token-time-to-live\":[\"java.time.Duration\",3600.000000000]}'),('da068f57-478c-4e35-bf0f-7ff0a7d5874c','messaging-client','2021-10-16 11:57:40','{bcrypt}$2a$10$Ye5W.zavGAIKG9WTrxsBle0ACXGRw/iuLQ1gyUkbYZZybA/394WAK',NULL,'da068f57-478c-4e35-bf0f-7ff0a7d5874c','client_secret_basic','refresh_token,client_credentials,authorization_code','http://127.0.0.1:8080/authorized,http://127.0.0.1:8080/login/oauth2/code/messaging-client-oidc','openid,message.read,message.write','{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"settings.client.require-proof-key\":false,\"settings.client.require-authorization-consent\":true}','{\"@class\":\"java.util.Collections$UnmodifiableMap\",\"settings.token.reuse-refresh-tokens\":true,\"settings.token.id-token-signature-algorithm\":[\"org.springframework.security.oauth2.jose.jws.SignatureAlgorithm\",\"RS256\"],\"settings.token.access-token-time-to-live\":[\"java.time.Duration\",300.000000000],\"settings.token.refresh-token-time-to-live\":[\"java.time.Duration\",3600.000000000]}');
/*!40000 ALTER TABLE `oauth2_registered_client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persistent_logins`
--

DROP TABLE IF EXISTS `persistent_logins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persistent_logins` (
  `username` varchar(64) COLLATE utf8mb4_general_ci NOT NULL,
  `series` varchar(64) COLLATE utf8mb4_general_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_general_ci NOT NULL,
  `last_used` timestamp NOT NULL,
  PRIMARY KEY (`series`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persistent_logins`
--

LOCK TABLES `persistent_logins` WRITE;
/*!40000 ALTER TABLE `persistent_logins` DISABLE KEYS */;
/*!40000 ALTER TABLE `persistent_logins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `userName` varchar(20) DEFAULT NULL,
  `password` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `enabled` tinyint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (2,'user1','{bcrypt}$2a$10$FvDcQN3oVj8zNPilxbYJ2uAEsmOw.zARnXEMxdzh7IHPLIoUDWc4q',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'sso'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-10-16 16:40:40
