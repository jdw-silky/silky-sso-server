package cn.silky.authorization.sys.controller;


import cn.silky.authorization.sys.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ListJiang
 * @since 2021-10-16
 */
@RestController
@RequestMapping("/oauth2-registered-client")
public class Oauth2RegisteredClientController {

    @Autowired
    private RegisteredClientRepository registeredClientRepository;

    /**
     * 通过client-id查询 有效oauth2注册客户端
     *
     * @return oauth2注册客户端
     */
    @GetMapping("/client-id/{clientId}")
    public RegisteredClient findByClientId(@PathVariable("clientId") String clientId) {
        return registeredClientRepository.findByClientId(clientId);
    }

    /**
     * 通过id查询oauth2注册客户端
     *
     * @param id id
     * @return oauth2注册客户端
     */
    @GetMapping("/{id}")
    public RegisteredClient findById(@PathVariable("id") String id) {
        return registeredClientRepository.findById(id);
    }

    /**
     * 幂等插入oauth2注册客户端
     *
     * @param client oauth2注册客户端
     * @return oauth2注册客户端
     */
    @PutMapping("/{id}")
    public Result save(@RequestBody RegisteredClient client) {
        try {
            registeredClientRepository.save(client);
            return Result.OK();
        } catch (Exception e) {
            return Result.error("save error");
        }
    }


}