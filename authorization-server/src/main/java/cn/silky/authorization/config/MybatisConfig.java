package cn.silky.authorization.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author ListJiang
 * @since description
 * datetime 2021/10/16 15:39
 */
@Configuration
@MapperScan(value = "cn.silky.authorization.*.mapper")
public class MybatisConfig {
}