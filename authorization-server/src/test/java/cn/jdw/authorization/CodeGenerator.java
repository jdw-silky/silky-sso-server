package cn.jdw.authorization;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.baomidou.mybatisplus.generator.fill.Column;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.security.RunAs;
import javax.sql.DataSource;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * @Author ListJiang
 * @Since class mybatis-plus代码生成器
 * datetime 2021/7/1 14:30
 */
public class CodeGenerator {

    private static String projectPath = "D:\\ideaProject\\gitee\\jdw-silky\\silky-sso-server\\authorization-server" +
            "\\src\\main\\java";
    private static String author = "ListJiang";
    private static String packageParent = "cn.silky.authorization.sys";
    private static String url = "jdbc:mysql://192.168.137.139:3306/sso?serverTimezone=GMT%2b8" +
            "&zeroDateTimeBehaviorconvertToNull&characterEncoding=utf8&useSSL=false&allowPublicKeyRetrieval=true";
    private static String driverName = "com.mysql.cj.jdbc.Driver";
    private static String user = "root";
    private static String password = "root";

    // 处理 all 情况
    protected static List<String> getTables(String tables) {
        return "all".equals(tables) ? Collections.emptyList() : Arrays.asList(tables.split(","));
    }

    public static void main(String[] args) {
        FastAutoGenerator.create(url, user, password)
                // 全局配置
                .globalConfig((scanner, builder) ->
                        builder.author(scanner.apply("请输入作者名称？"))
                                .fileOverride()
                                .outputDir(projectPath))
                // 包配置
                .packageConfig((scanner, builder) -> builder.parent(scanner.apply("请输入包名？")))
                // 策略配置
                .strategyConfig((scanner, builder) -> builder.addInclude(getTables(scanner.apply("请输入表名，多个英文逗号分隔？所有输入" +
                                " all")))
                        .controllerBuilder().enableRestStyle().enableHyphenStyle()
                        .entityBuilder().enableLombok()
                        .build())
                .templateEngine(new FreemarkerTemplateEngine())
                .execute();
    }
}